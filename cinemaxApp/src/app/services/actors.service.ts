import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ActorsService {

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  getAllActors$() {
    return this.httpClient.get(environment.url + '/api/actors')
  }

  getActorById$(actorId: string) {
    return this.httpClient.get(environment.url + '/api/actors/' + actorId)
  }

  modifyActor$(actorId: string, data) {
    return this.httpClient.put(environment.url + '/api/actors/' + actorId, data)
  }

  postActor$(data) {
    return this.httpClient.post(environment.url + '/api/actors/', data)
  }

  deleteActor$(actorId) {
    return this.httpClient.delete(environment.url + '/api/actors/' + actorId)
  }
}
