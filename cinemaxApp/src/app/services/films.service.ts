import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class FilmsService {

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  getAllFilms$() {
    return this.httpClient.get(environment.url + '/api/movies')
  }

  getMovieById$(movieId: string) {
    return this.httpClient.get(environment.url + '/api/movies/' + movieId)
  }

  postFilm$(dataForm) {
    return this.httpClient.post(environment.url + '/api/movies', dataForm)
  }

  modifyMovie$(movieId: string, data) {
    return this.httpClient.put(environment.url + '/api/movies/' + movieId, data)
  }

  deleteMovie$(movieId) {
    return this.httpClient.delete(environment.url + '/api/movies/' + movieId)
  }
}
