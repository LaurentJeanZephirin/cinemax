import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayActorsComponent } from './display-actors.component';

describe('DisplayActorsComponent', () => {
  let component: DisplayActorsComponent;
  let fixture: ComponentFixture<DisplayActorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayActorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayActorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
