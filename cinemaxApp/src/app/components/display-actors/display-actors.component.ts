import {Component, Input, OnInit} from '@angular/core';
import {ActorModel} from "../../models/actorModel";
import {ActorsService} from "../../services/actors.service";
import {tap} from "rxjs/operators";
import {combineLatest} from "rxjs";

@Component({
  selector: 'app-display-actors',
  templateUrl: './display-actors.component.html',
  styleUrls: ['./display-actors.component.scss']
})
export class DisplayActorsComponent implements OnInit {

  @Input() actors: string[];
  data: ActorModel[] = [];

  constructor(
    private actorsService: ActorsService,
  ) {
  }

  ngOnInit() {
    const promiseToResolve = this.actors.map(actors => {
      return this.actorsService.getActorById$(actors)
        .pipe(
          tap(actor => this.data.push(actor as ActorModel))
        )
    });
    combineLatest(promiseToResolve).subscribe()
  }

}
