import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DisplayActorsComponent} from "./display-actors.component";
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';

@NgModule({
  declarations: [
    DisplayActorsComponent
  ],
  imports: [
    CommonModule,
    MatListModule,
    MatCardModule,
    MatDividerModule,
  ],
  exports: [
    DisplayActorsComponent
  ]
})
export class DisplayActorsModule { }
