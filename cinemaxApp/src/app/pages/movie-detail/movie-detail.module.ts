import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MovieDetailComponent} from "./movie-detail.component";
import {MovieDetailRoutingModule} from "./movie-detail.routing.module";
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from "@angular/forms";
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {DisplayActorsModule} from "../../components/display-actors/display-actors.module";


@NgModule({
  declarations: [
    MovieDetailComponent
  ],
  imports: [
    CommonModule,
    MovieDetailRoutingModule,
    MatInputModule,
    ReactiveFormsModule,
    MatCardModule,
    MatDividerModule,
    MatButtonModule,
    MatSelectModule,
    MatCheckboxModule,
    DisplayActorsModule,
  ]
})
export class MovieDetailModule {
}
