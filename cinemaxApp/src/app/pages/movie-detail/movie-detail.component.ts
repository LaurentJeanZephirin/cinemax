import {Component, OnInit} from '@angular/core';
import {FilmModel} from "../../models/filmModel";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FilmsService} from "../../services/films.service";
import {ActivatedRoute, Router} from "@angular/router";
import {map, switchMap, tap} from "rxjs/operators";
import {ActorsService} from "../../services/actors.service";
import {ActorModel} from "../../models/actorModel";
import {of} from "rxjs";

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {

  film: FilmModel;
  filmForm: FormGroup;
  filmId: string;
  actors: ActorModel[];

  constructor(
    private filmsService: FilmsService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private actorsService: ActorsService,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.route.params
      .pipe(
        map(params => this.filmId = params.id),
        switchMap(filmId => this.filmsService.getMovieById$(filmId)),
        tap(film => {
          this.film = film as FilmModel;
          this._initForm(this.film)
        }),
      )
      .subscribe();
    this.getAllActors();
  }

  getAllActors() {
    this.actorsService.getAllActors$()
      .pipe(
        tap(actors => this.actors = actors as ActorModel[])
      )
      .subscribe()
  }

  modifyFilm() {
    this.filmsService.modifyMovie$(this.filmId, this.filmForm.value)
      .pipe(
        tap(() => this.router.navigate(['movie_list']))
      )
      .subscribe()
  }

  deleteFilm() {
    this.filmsService.deleteMovie$(this.filmId)
      .pipe(
        tap(() => this.router.navigate(['movie_list']))
      )
      .subscribe()
  }

  confirmMessage() {
    const confirmed = confirm('Etes vous certain de vouloir supprimer: ' + this.film.title + ' ?');
    if (confirmed) {
      this.deleteFilm()
    } else {
      return of(null);
    }
  }

  private _initForm(film: FilmModel) {
    this.filmForm = this.fb.group({
      title: [film.title, Validators.required],
      category: [film.category, Validators.required],
      releaseYear: [film.releaseYear, Validators.required],
      poster: [film.poster, Validators.required],
      directors: [film.directors, Validators.required],
      actors: [film.actors, Validators.required],
      synopsis: [film.synopsis, Validators.required],
      rate: [film.rate, Validators.required],
      lastViewDate: [film.lastViewDate, Validators.required],
      price: [film.price, Validators.required],
    })
  }

}
