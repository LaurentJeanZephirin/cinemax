import {Component, OnInit} from '@angular/core';
import {ActorsService} from "../../services/actors.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-actor-create',
  templateUrl: './actor-create.component.html',
  styleUrls: ['./actor-create.component.scss']
})
export class ActorCreateComponent implements OnInit {

  actorForm: FormGroup;

  constructor(
    private actorsService: ActorsService,
    private fb: FormBuilder,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this._initForm()
  }

  postActor() {
    this.actorsService.postActor$(this.actorForm.value)
      .pipe(
        tap(() => this.router.navigate(['actor_list']))
      )
      .subscribe()
  }

  private _initForm() {
    this.actorForm = this.fb.group({
      name: ['', Validators.required]
    })
  }

}
