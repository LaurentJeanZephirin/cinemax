import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ActorCreateComponent} from "./actor-create.component";
import {ActorCreateRoutingModule} from "./actor-create.routing.module";
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    ActorCreateComponent
  ],
  imports: [
    CommonModule,
    ActorCreateRoutingModule,
    MatButtonModule,
    MatInputModule,
    MatDividerModule,
    MatCardModule,
    ReactiveFormsModule,
  ]
})
export class ActorCreateModule {
}
