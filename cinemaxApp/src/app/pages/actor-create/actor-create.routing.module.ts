import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ActorCreateComponent} from "./actor-create.component";


const routes: Routes = [
  {
    path: '',
    component: ActorCreateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActorCreateRoutingModule {
}
