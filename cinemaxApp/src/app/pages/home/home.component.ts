import {Component, OnInit} from '@angular/core';
import {tap} from "rxjs/operators";
import {FilmsService} from "../../services/films.service";
import {ActorsService} from "../../services/actors.service";
import {ActorModel} from "../../models/actorModel";
import {FilmModel} from "../../models/filmModel";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  films: FilmModel[];
  actors: ActorModel[];

  constructor(
    private filmsService: FilmsService,
    private actorsService: ActorsService,
  ) {
  }

  ngOnInit() {
    this.getAllFilms();
    this.getAllActors();
  }


  getAllFilms() {
    this.filmsService.getAllFilms$()
      .pipe(
        tap(films => this.films = films as FilmModel[])
      )
      .subscribe()
  }

  getAllActors() {
    this.actorsService.getAllActors$()
      .pipe(
        tap(actors => this.actors = actors as ActorModel[])
      )
      .subscribe()
  }

}

