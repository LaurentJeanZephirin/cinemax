import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MovieListComponent} from "./movie-list.component";
import {MovieListRoutingModule} from "./movie-list.routing.module";
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';


@NgModule({
  declarations: [
    MovieListComponent
  ],
  imports: [
    CommonModule,
    MovieListRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatInputModule,
  ]
})
export class MovieListModule {
}
