import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material";
import {FilmsService} from "../../services/films.service";
import {tap} from "rxjs/operators";
import {FilmModel} from "../../models/filmModel";

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {

  displayedColumns: string[] = ['title', 'category', 'releaseYear', 'directors', 'rate', 'price', 'detail'];
  dataSource = new MatTableDataSource();

  constructor(
    private filmsService: FilmsService,
  ) {
  }

  ngOnInit() {
    this.getAllFilms()
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getAllFilms() {
    this.filmsService.getAllFilms$()
      .pipe(
        tap(films => this.dataSource.data = films as FilmModel[])
      )
      .subscribe()
  }

}
