import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ActorListComponent} from "./actor-list.component";
import {ActorListRoutingModule} from "./actor-list.routing.module";
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    ActorListComponent
  ],
  imports: [
    CommonModule,
    ActorListRoutingModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
  ]
})
export class ActorListModule {
}
