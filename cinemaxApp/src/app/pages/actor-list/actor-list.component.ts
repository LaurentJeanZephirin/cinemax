import {Component, OnInit} from '@angular/core';
import {ActorModel} from "../../models/actorModel";
import {ActorsService} from "../../services/actors.service";
import {tap} from "rxjs/operators";
import {MatTableDataSource} from "@angular/material";

@Component({
  selector: 'app-actor-list',
  templateUrl: './actor-list.component.html',
  styleUrls: ['./actor-list.component.scss']
})
export class ActorListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'detail'];
  dataSource = new MatTableDataSource();

  constructor(
    private actorsService: ActorsService,
  ) {
  }

  ngOnInit() {
    this.getAllActors()
  }

  getAllActors() {
    this.actorsService.getAllActors$()
      .pipe(
        tap(actors => this.dataSource.data = actors as ActorModel[])
      )
      .subscribe()
  }

}
