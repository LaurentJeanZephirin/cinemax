import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ActorDetailComponent} from "./actor-detail.component";
import {ActorDetailRoutingModule} from "./actor-detail.routing.module";
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    ActorDetailComponent
  ],
  imports: [
    CommonModule,
    ActorDetailRoutingModule,
    MatCardModule,
    MatDividerModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
  ]
})
export class ActorDetailModule {
}
