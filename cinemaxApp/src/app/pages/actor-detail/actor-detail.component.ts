import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {map, switchMap, tap} from "rxjs/operators";
import {ActorsService} from "../../services/actors.service";
import {ActorModel} from "../../models/actorModel";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {of} from "rxjs";

@Component({
  selector: 'app-actor-detail',
  templateUrl: './actor-detail.component.html',
  styleUrls: ['./actor-detail.component.scss']
})
export class ActorDetailComponent implements OnInit {

  actor: ActorModel;
  actorForm: FormGroup;
  actorId: string;

  constructor(
    private route: ActivatedRoute,
    private actorsService: ActorsService,
    private fb: FormBuilder,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.route.params
      .pipe(
        map(params => this.actorId = params.id),
        switchMap(actorId => this.actorsService.getActorById$(actorId)),
        tap(actor => {
          this.actor = actor as ActorModel;
          this._initForm(this.actor)
        }),
      )
      .subscribe();
  }

  modifyActor() {
    this.actorsService.modifyActor$(this.actorId, this.actorForm.value)
      .pipe(
        tap(() => this.router.navigate(['actor_list']))
      )
      .subscribe()
  }

  deleteActor() {
    this.actorsService.deleteActor$(this.actorId)
      .pipe(
        tap(() => this.router.navigate(['actor_list']))
      )
      .subscribe()
  }

  confirmMessage() {
    const confirmed = confirm('Etes vous certain de vouloir supprimer: ' + this.actor.name + ' ?');
    if (confirmed) {
      this.deleteActor()
    } else {
      return of(null);
    }
  }

  private _initForm(actor?: ActorModel) {
    this.actorForm = this.fb.group({
      name: [actor.name, Validators.required],
    })
  }

}
