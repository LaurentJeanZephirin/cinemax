import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FilmsService} from "../../services/films.service";
import {tap} from "rxjs/operators";
import {ActorsService} from "../../services/actors.service";
import {ActorModel} from "../../models/actorModel";
import {Router} from "@angular/router";


@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.scss']
})
export class AddMovieComponent implements OnInit {

  addMovieForm: FormGroup;
  actors: ActorModel[];

  constructor(
    private fb: FormBuilder,
    private filmsService: FilmsService,
    private actorsService: ActorsService,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this._initForm();
    this.getAllActors();
  }

  postFilm() {
    this.filmsService.postFilm$(this.addMovieForm.value)
      .pipe(
        tap(() => this.router.navigate(['movie_list']))
      )
      .subscribe()
  }

  getAllActors() {
    this.actorsService.getAllActors$()
      .pipe(
        tap(actors => this.actors = actors as ActorModel[])
      )
      .subscribe()
  }

  private _initForm() {
    this.addMovieForm = this.fb.group({
      title: ['', Validators.required],
      category: ['', Validators.required],
      releaseYear: ['', Validators.required],
      poster: ['', Validators.required],
      directors: ['', Validators.required],
      actors: [],
      synopsis: ['', Validators.required],
      rate: [0, Validators.required],
      lastViewDate: ['', Validators.required],
      price: [0, Validators.required],
    })
  }

}
