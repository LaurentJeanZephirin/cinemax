import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'add_movie',
    loadChildren: () => import('./pages/add-movie/add-movie.module').then(m => m.AddMovieModule)
  },
  {
    path: 'movie_list',
    loadChildren: () => import('./pages/movie-list/movie-list.module').then(m => m.MovieListModule)
  },
  {
    path: 'movie_detail',
    loadChildren: () => import('./pages/movie-detail/movie-detail.module').then(m => m.MovieDetailModule)
  },
  {
    path: 'actor_list',
    loadChildren: () => import('./pages/actor-list/actor-list.module').then(m => m.ActorListModule)
  },
  {
    path: 'actor_detail',
    loadChildren: () => import('./pages/actor-detail/actor-detail.module').then(m => m.ActorDetailModule)
  },
  {
    path: 'actor_create',
    loadChildren: () => import('./pages/actor-create/actor-create.module').then(m => m.ActorCreateModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
