export interface FilmModel {
  id: number,
  title: string,
  category: string,
  releaseYear: string,
  poster: string,
  directors: string,
  actors: string[],
  synopsis: string,
  rate: number,
  lastViewDate: string,
  price: number,
}
